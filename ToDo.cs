﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace LV1_RPPOON
{
    class ToDo
    {
        private List<NoteWithDate> notes;


        public ToDo()
        {
            this.notes = new List<NoteWithDate>();
        }
        public ToDo(List<NoteWithDate> notes)
        {
            this.notes = new List<NoteWithDate>(notes);
        }


        public void AddNote(NoteWithDate note)
        {
            notes.Add(note);
        }
        public bool RemoveNote(NoteWithDate note)
        {
            bool removed = notes.Remove(note);
            return removed;
        }
        
        
        public NoteWithDate getNote(int index)
        {
            NoteWithDate note = this.notes[index];
            return note;
        }
        
        
        public void printToConsole()
        {
            foreach (NoteWithDate note in notes)
            {
                Console.WriteLine(note.ToString());
            }
        }
        public void printToFile(string filename)
        {
            foreach (NoteWithDate note in notes)
            {
                File.WriteAllText(filename, note.ToString());
            }
        }
        
    }
}
