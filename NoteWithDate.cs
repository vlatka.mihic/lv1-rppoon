﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1_RPPOON
{
    class NoteWithDate: Note
    {
        private DateTime date { get; set; }

        public NoteWithDate(): base()
        {
            this.date = DateTime.UtcNow;
        }
        public NoteWithDate(string authorName, Importance importanceLevel) : base(authorName, importanceLevel)
        {
            this.date = DateTime.UtcNow;
        }
        public NoteWithDate(string authorName, Importance importanceLevel, DateTime date) : base(authorName, importanceLevel)
        {
            this.date = date;
        }
        public NoteWithDate(string textContent, string authorName, Importance importanceLevel, DateTime date) 
        : base(textContent, authorName, importanceLevel)
        {
            this.date = date;
        }

        public override string ToString()
        {
            return "(" + this.date.ToString() + ")" + this.author + ": " + this.text;
        }
       
    }
}
