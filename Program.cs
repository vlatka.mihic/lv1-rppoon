﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LV1_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            
            ToDo todolist = new ToDo();

            List<string> authorsNames = new List<string>();
            List<Importance> notesImportances = new List<Importance>();

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Upisite autora " + (i + 1).ToString() + ". biljeske: ");
                authorsNames.Add(Console.ReadLine());

                Console.WriteLine("Upisite vaznost " + (i + 1).ToString() + ". biljeske(Low, Medium or High): ");
                string importance = Console.ReadLine();
                if (importance == "High") { notesImportances.Add(Importance.High); }
                else if (importance == "Medium") { notesImportances.Add(Importance.Medium); }
                else { notesImportances.Add(Importance.Low); }
            }

            NoteWithDate firstNote = new NoteWithDate(authorsNames[0], notesImportances[0]);
            NoteWithDate secondNote = new NoteWithDate(authorsNames[1], notesImportances[1]);
            NoteWithDate thirdNote = new NoteWithDate(authorsNames[2], notesImportances[2]);

            Console.WriteLine("Upisite teks 1. biljeske: ");
            firstNote.text = Console.ReadLine();

            Console.WriteLine("Upisite teks 2. biljeske: ");
            secondNote.text = Console.ReadLine();

            Console.WriteLine("Upisite teks 3. biljeske: ");
            thirdNote.text = Console.ReadLine();

            todolist.AddNote(firstNote);
            todolist.AddNote(secondNote);
            todolist.AddNote(thirdNote);

            todolist.printToConsole();

            int n = 3;
            for (int i = 0; i < n; i++)
            {
                if (todolist.getNote(i).getImportance() == Importance.High)
                {
                    todolist.RemoveNote(todolist.getNote(i));
                    i--;
                    n--;
                }
            }


            Console.WriteLine("Jos za obaviti: ");
            todolist.printToConsole();

            Console.ReadKey();
        }
    }
}
