﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1_RPPOON
{
    enum Importance
    {
        Low,
        Medium,
        High
    }
    class Note
    {
        public string text { get; set; }
        public string author { get; private set; }
        public Importance importance { get; set; }

        public Note()
        {
            text = "Text";
            author = "Anonymous";
            importance = Importance.Low;
        }

        public Note(string authorName, Importance importanceLevel)
        {
            text = " ";
            author = authorName;
            importance = importanceLevel;
        }

        public Note(string textContent, string authorName, Importance importanceLevel)
        {
            text = textContent;
            author = authorName;
            importance = importanceLevel;
        }


        public string getText()
        {
            return this.text;
        }
        public string getAuthor()
        {
            return this.author;
        }
        public Importance getImportance()
        {
            return this.importance;
        }
        public void setText(string text)
        {
            this.text = text;
        }
        public void setImportance(Importance importance)
        {
            this.importance = importance;
        }

        public override string ToString()
        {
            return this.getAuthor() + ": " + this.getText();
        }
    }
}
